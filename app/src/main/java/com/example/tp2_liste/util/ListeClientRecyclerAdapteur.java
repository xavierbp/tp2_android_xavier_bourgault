package com.example.tp2_liste.util;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp2_liste.Item;
import com.example.tp2_liste.R;

import java.util.ArrayList;

public class ListeClientRecyclerAdapteur extends RecyclerView.Adapter<ListeClientRecyclerAdapteur.ViewHolder>{

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvNomItem,tvQuantitee;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            tvNomItem = itemView.findViewById(R.id.tv_nom_item);
            tvQuantitee = itemView.findViewById(R.id.tv_quantitee);


        }
    }
    private ArrayList<Pair<Item,Integer>> itemsClient;
    public ListeClientRecyclerAdapteur(ArrayList<Pair<Item,Integer>> items){this.itemsClient = items;}


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_liste_client,parent,false);
        return new ListeClientRecyclerAdapteur.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pair<Item,Integer> item = itemsClient.get(position);
        holder.tvNomItem.setText(item.first.getM_Nom());
        holder.tvQuantitee.setText(String.valueOf(item.second));

    }

    @Override
    public int getItemCount() {
        if(itemsClient !=null) {
            return itemsClient.size();
        }else{
            return 0;
        }
    }
}
