package com.example.tp2_liste.util;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp2_liste.Item;
import com.example.tp2_liste.R;

import java.util.ArrayList;

public class ListeVendeurRecyclerAdapteur extends RecyclerView.Adapter<ListeVendeurRecyclerAdapteur.ViewHolder> {
    private onItemClickListener mListener;
    //interface onItemClickListener
    public interface onItemClickListener{
        void onItemClick(int position);

        //void onClickDelete(int position);
    }

    public void setOnClickListener(onItemClickListener listener){
        this.mListener = listener;
    }
    //public void setOnLongClickListener(onItemClickListener listener){this.mListener = listener;}
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvNom, tvDescription,tvPrix,tvCategorie;
        public ImageView ivProduit;



        public ViewHolder(@NonNull View itemView,final onItemClickListener listener){
            super(itemView);
            tvNom = itemView.findViewById(R.id.tv_nom);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvPrix = itemView.findViewById(R.id.tv_prix);
            tvCategorie = itemView.findViewById(R.id.tv_categorie);

            ivProduit= itemView.findViewById(R.id.iv_produit);
            //le textview n'est pas visible et n'occupe aucun espace dans le layout

            itemView.setOnClickListener(new OnClickListener(){

                @Override
                public void onClick(View view) {
                    if(listener !=null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
    private ArrayList<Item> items;
    public ListeVendeurRecyclerAdapteur(ArrayList<Item> items){
        this.items = items;
    }

    @NonNull
    @Override
    //méthode qui crée la ligne
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //on "inflate" le layout des lignes avec le layout row_liste_vendeur
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_liste_vendeur,parent,false);
        //on retourne la ligne
        return new ViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //on assigne la valeur de l'objet à la position spécifié à item
        Item item = items.get(position);
        //on assigne la valeur du nom de l'item au textView correspondant
        holder.tvNom.setText(item.getM_Nom());
        //on assigne la valeur de la description de l'item au textView correspondant
        holder.tvDescription.setText(item.getM_Description());
        //on assigne la valeur du prix de l'item au textView correspondant
        holder.tvPrix.setText(String.valueOf(item.getM_Prix()));
        //on assigne la valeur de la categorie au tv correspondant
        holder.tvCategorie.setText(item.getM_Categorie());
        if(position%2 ==0){
            holder.ivProduit.setImageResource(R.drawable.image_1);
        }else{
            holder.ivProduit.setImageResource(R.drawable.image_2);
        }
    }

    @Override
    //retourne le nombre d'items dans la liste
    public int getItemCount() {

        return items.size();
    }
}
