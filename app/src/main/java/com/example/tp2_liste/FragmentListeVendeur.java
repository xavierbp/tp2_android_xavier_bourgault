package com.example.tp2_liste;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp2_liste.data.AppExecutors;
import com.example.tp2_liste.data.ItemRoomDB;
import com.example.tp2_liste.util.ListeVendeurRecyclerAdapteur;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class FragmentListeVendeur extends Fragment implements View.OnClickListener{
    //liste reçu de l'activitée principale sera initialisé par le bundle
    ArrayList<Item> listeItemsVendeur;
    //un ArrayList qui va contenir une paire de donnée combinaison clé valeurs Item, et la quantitée de celui-ci
    ArrayList<Pair<Item,Integer>> listeItemClients;
    FloatingActionButton fabAjout;
    FragmentListeClient listeClient;
    FragmentManager manager;
    FragmentTransaction transaction;
    ItemRoomDB mDb;
    private Bundle bundle,bundle2;
    private RecyclerView listeVendeur;
    private ListeVendeurRecyclerAdapteur adapteurVendeur;
    //valeur qui sera fournit par le bundle
    private  Boolean estAdmin;
    private static final String TAG = FragmentListeVendeur.class.getSimpleName();
    public FragmentListeVendeur (){
        Log.d(TAG, "FragmentListeVendeur: -------------------");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //par défaut estAdmin sera à faux
        estAdmin = false;
        listeItemClients = new ArrayList<Pair<Item,Integer>>();
        mDb = ItemRoomDB.getDatabase(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liste_vendeur,container,false);
        //instanciation des éléments dans le layout
        listeVendeur = (RecyclerView) view.findViewById(R.id.rv_listeVendeur);
        fabAjout = (FloatingActionButton) view.findViewById(R.id.fab_ajout_items);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //creation d'un bundle pour recupérer les infos passez par l'activité
         bundle = getArguments();
        if(bundle != null){
            //si le bundle contient la clé "estAdmin" on assigne sa valeur au booléen estAdmin
            if (bundle.containsKey("estAdmin")) {
                estAdmin = bundle.getBoolean("estAdmin");
            }
            //si le bundle contient la liste alors celle-ci sera mises à jour
            if (bundle.containsKey("listeItemsVendeur")){
                listeItemsVendeur = (ArrayList<Item>) bundle.getSerializable("listeItemsVendeur");
            }
        }
        //si la variable admin est à vrai on affiche le floatActionButton
        if(estAdmin){
            fabAjout.show();
        }else{
            fabAjout.hide();
        }
        //Instanciation de l'adapteur
        adapteurVendeur = new ListeVendeurRecyclerAdapteur(listeItemsVendeur);
        //Passage du recyclerview adapter au recycler view
        listeVendeur.setAdapter(adapteurVendeur);
        listeVendeur.setLayoutManager(new LinearLayoutManager(getActivity()));
        //surcharge de la méthode setOnClickListener nécessaire pour un Recycler View
        adapteurVendeur.setOnClickListener(new ListeVendeurRecyclerAdapteur.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Log.e(TAG, "onItemClick: click" );
                if(!estAdmin){
                // on ajoute l'item à la position cliquer dans le paire avec une quantitée de 1 dans la nouvelle liste
                    if(listeItemClients !=null && listeItemClients.isEmpty()){
                        //si la liste n'a pas été initialisé
                        listeItemClients.add(new Pair<Item,Integer>(listeItemsVendeur.get(position),1));
                    }else{
                        //sinon on vérifie s'il est ou non dans la liste
                        Boolean estPresent = false;
                        Log.e(TAG, "onItemClick: liste existante" );
                        for (Pair<Item,Integer> i:listeItemClients
                        ) {if(i.first == listeItemsVendeur.get(position)){
                            estPresent = true;
                            //on remplace l'objet pair par un objet pair dont le second est incrémenté de 1

                            listeItemClients.set(listeItemClients.indexOf(i),new Pair<Item,Integer>(i.first,(i.second+1)));

                        }
                        if(!estPresent){
                            listeItemClients.add(new Pair<Item,Integer>(listeItemsVendeur.get(position),1));
                        }

                        }
                    }
                    Log.e(TAG, "onItemClick: listeItemClients"+listeItemClients);
                    //creation d'un nouveau bundle pour passez au 2e fragment
                    bundle2 = new Bundle();
                    bundle2.putSerializable("listeItemClients",listeItemClients);
                    //création d'un nouveau fragment pour remplacer celui actuellement affiché dans l'activité principale
                    listeClient = new FragmentListeClient();
                    listeClient.setArguments(bundle2);
                    manager = getParentFragmentManager();
                    transaction = manager.beginTransaction();
                    //remplacement du nouveau fragment dans l'activité principale
                    transaction.replace(R.id.fl_fragment2,listeClient);
                    //fermeture de la transaction
                    transaction.commit();


                    }

                else{
                    Log.e(TAG, "onItemClick: Click Admin" );
                    PopupMenu popupMenu = new PopupMenu(getActivity(),view);
                    popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
                    //variable pour stocker l'item

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            switch(menuItem.getItemId()){
                                case R.id.item_mod:
                                    //code pour la modification ici
                                    /*//apparition de l'interface de modification
                                    View modView = getLayoutInflater().inflate(R.layout.set_item, null);
                                    //TODO: ajouter un listener
                                    new AlertDialog.Builder(getActivity())
                                            .setTitle("Nouvel Item")
                                            .setView(modView)
                                            .setNegativeButton("Annuler", null)
                                            .setPositiveButton("Ok", null)
                                            .show();*/
                                case R.id.item_del:
                                    AppExecutors.getInstance().diskIO().execute(
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    //suppresion à la bd du nouvel item
                                                    // je récupère la liste de tous les items
                                                    List<Item>items = mDb.ItemDao().getItems();
                                                    //je récupère l'item à positon ou j'ai cliquer
                                                    Item item = items.get(position);
                                                    //en utilisant la propriété de l'item id je le supprime de la base de donnée
                                                    mDb.ItemDao().delete(item.getM_id());
                                                }


                                            });

                            }
                            return false;
                        }
                    });


                }

            }
        });



        fabAjout.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab_ajout_items:
                //creation de la vue poure l'affichage de la fenêtr d'ajout
                View setView = getLayoutInflater().inflate(R.layout.set_item, null);
                //instanciation et initialisation des edits texts
                EditText etNom,etCategorie,etDescription,etQte,etPrix;
                etNom = setView.findViewById(R.id.et_nom);
                etCategorie = setView.findViewById(R.id.et_categorie);
                etDescription = setView.findViewById(R.id.et_description);
                etPrix = setView.findViewById(R.id.et_prix);
                etQte = setView.findViewById(R.id.et_qte);
                //creation d'un bouton qui sera appeler lors du click sur ok et qui inserera les éléments dans la liste et la BD
                BtnItemHandler btnItemHandler = new BtnItemHandler(etNom,etCategorie,etDescription,etQte,etPrix);
                new AlertDialog.Builder(getActivity())
                        .setTitle("Nouvel Item")
                        .setView(setView)
                        .setNegativeButton("Annuler", null)
                        .setPositiveButton("Ok", btnItemHandler)
                        .show();



        }

    }

    private class BtnItemHandler implements DialogInterface.OnClickListener {
        private EditText etNom,etCategorie,etDescription,etQte,etPrix;

        public BtnItemHandler(EditText et_nom,EditText et_Categorie,EditText et_description,EditText et_qte,EditText et_prix){
            this.etNom = et_nom;
            this.etCategorie = et_Categorie;
            this.etDescription = et_description;
            this.etQte = et_qte;
            this.etPrix = et_prix;
        }
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            Integer prix,qte;

            prix = Integer.valueOf(String.valueOf(etPrix.getText()));
            qte = Integer.valueOf(String.valueOf(etQte.getText()));
            if (qte != null || qte>1){
                qte = 1;
            }

            Item nouvItem = new Item(String.valueOf(etNom.getText()),String.valueOf(etDescription.getText()),
                    String.valueOf(etCategorie.getText()),prix,qte);
            AppExecutors.getInstance().diskIO().execute(
                    new Runnable() {
                        @Override
                        public void run() {
                            //ajout à la bd du nouvel item
                            mDb.ItemDao().Insert(nouvItem);
                        }


            });
            //ajout à la liste listeItemVendeurs
            listeItemsVendeur.add(nouvItem);




        }
    }
}
