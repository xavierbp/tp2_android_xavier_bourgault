package com.example.tp2_liste;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp2_liste.util.ListeClientRecyclerAdapteur;

import java.util.ArrayList;

public class FragmentListeClient extends Fragment {
    //private ListView listeClient;
    private RecyclerView listeClient;
    private ListeClientRecyclerAdapteur adapteurClient;
    ArrayList<Pair<Item,Integer>> listeItemClient;
    Integer total;
    TextView tvTotal;
    Bundle bundle;
    private static final String TAG = FragmentListeClient.class.getSimpleName();
    public FragmentListeClient (){
        Log.d(TAG, "FragmentA: -------------------");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        total = 0;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liste_client,container,false);
        listeClient = view.findViewById(R.id.rv_listeClient);
        tvTotal = view.findViewById(R.id.tv_total);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //TODO: prendre le bundle de la transaction et l'assigner à listeItemClient
        bundle = getArguments();
        if(bundle != null){
            //si le bundle contient la clé "listeItemCLient" on assigne sa valeur a la liste correspondante
            Log.e(TAG, "onViewCreated: bundle"+bundle );
            if (bundle.containsKey("listeItemClients")) {
                listeItemClient = (ArrayList<Pair<Item,Integer>>) bundle.getSerializable("listeItemClients");
            }
        }
        //on crée l'adapteur en lui passant la liste
        adapteurClient = new ListeClientRecyclerAdapteur(listeItemClient);
        //on l'assigne au recycler view
        listeClient.setAdapter(adapteurClient);
        listeClient.setLayoutManager(new LinearLayoutManager(getActivity()));



    }

    @Override
    public void onResume() {
        super.onResume();
        if(listeItemClient != null){
            if(listeItemClient.isEmpty()) {
                total = 0;
            }else{
                for (Pair<Item,Integer> i: listeItemClient) {
                    //le premier argument de la pair est l'item donc on va chercher son prix que l'on multiplie avec le second item la quantitée
                    total += (i.first.getM_Prix()*i.second);

                }
            }
        }else{
            total = 0;
        }
        //affichage du total
        tvTotal.setText(String.valueOf(total));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //vider la liste lors de la destruction du fragment
        listeItemClient= null;
    }


}
