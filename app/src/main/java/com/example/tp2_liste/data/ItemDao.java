package com.example.tp2_liste.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.tp2_liste.Item;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ItemDao {
    @Insert
    //ajout d'un item à DB en lui passant un objetde type "item"
    Void Insert(Item item);
    //requête SQL qui retourne toutes les lignes de la table
    @Query("SELECT * FROM item_table")
    //getItems retourne une liste de tout les items dans la db
    List<Item> getItems();
    //méthode pour supprimer un item selon son nom (clé primaire de la table)
    @Query("DELETE FROM item_table WHERE id=:id")
    void delete(int id);
    //mettre à jour un item en lui passant un objet item avec un id definit pour que le DOA puisse l'identifier dans la table
    @Update
    int update(Item item);


}
