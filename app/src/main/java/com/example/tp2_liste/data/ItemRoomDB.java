package com.example.tp2_liste.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.tp2_liste.Item;

@Database(entities ={Item.class},version=1)
public abstract class ItemRoomDB extends RoomDatabase {
    //Singleton
    public static  ItemRoomDB INSTANCE;

    //DAO
    public abstract ItemDao ItemDao();

    public static synchronized ItemRoomDB getDatabase(final Context context){
        //si la BD n'existe pas
        if(INSTANCE == null){
            // on créée la bd
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ItemRoomDB.class,"Item_Database").build();

        }
        //on retourne la BD
        return INSTANCE;
    }
}
