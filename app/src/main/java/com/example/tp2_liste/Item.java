package com.example.tp2_liste;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//classe représentant un objet item en java avec ses attributs
//nom de la table qui contiendra les objets items
@Entity(tableName = "item_table")
public class Item {
    //la clé primère de mon item dans la liste
    //la clé primaire sera le nom de l'item
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    private int m_id;
    @ColumnInfo(name="nom_col")
    private String m_Nom;

    public void setM_id(int m_id) {
        this.m_id = m_id;
    }

    public void setM_Nom(String m_Nom) {
        this.m_Nom = m_Nom;
    }

    public void setM_Description(String m_Description) {
        this.m_Description = m_Description;
    }

    public void setM_Categorie(String m_Categorie) {
        this.m_Categorie = m_Categorie;
    }

    public void setM_Prix(int m_Prix) {
        this.m_Prix = m_Prix;
    }

    public void setM_Quantitee(int m_Quantitee) {
        this.m_Quantitee = m_Quantitee;
    }

    @ColumnInfo(name = "description_col")
    private String m_Description;
    @ColumnInfo(name = "categorie_col")
    private String m_Categorie;
    @ColumnInfo(name = "prix_col")
    private int m_Prix;
    @ColumnInfo(name = "quantitee_col")
    private int m_Quantitee;
    //constructeur vide pour la création de liste d'Item non-initialisée
    public Item(){
    }
    //constructeur avec la quantitée non-spécifiée
    public Item(String nom,String description,String categorie,int prix){
        this.m_Nom = nom;
        this.m_Description = description;
        this.m_Categorie = categorie;
        this.m_Prix = prix;
        this.m_Quantitee = 1;

    }
    //constructeur de l'pbjet item si la quantitée est spécifée
    public Item(String nom,String description,String categorie,int prix,int quantitee){
        this.m_Nom = nom;
        this.m_Description = description;
        this.m_Categorie = categorie;
        this.m_Prix = prix;
        this.m_Quantitee = quantitee;
    }

    public String getM_Nom() {
        return m_Nom;
    }

    public String getM_Description() {
        return m_Description;
    }

    public String getM_Categorie() {
        return m_Categorie;
    }

    public int getM_Prix() {
        return m_Prix;
    }

    public int getM_Quantitee() {
        return m_Quantitee;
    }
    public int getM_id(){
        return m_id;
    }
}
