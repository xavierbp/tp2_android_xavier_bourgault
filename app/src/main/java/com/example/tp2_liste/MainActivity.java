package com.example.tp2_liste;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.tp2_liste.data.AppExecutors;
import com.example.tp2_liste.data.ItemRoomDB;
import com.example.tp2_liste.util.ListeVendeurRecyclerAdapteur;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener  {
    //booleen qui détermine si le mode admin est activé
    boolean estAdmin;
    Toolbar toolbar;
    ItemRoomDB mDb;
    Bundle bundle;
    ArrayList<Item> listeItemsVendeur;
    FrameLayout fragmentVendeur, fragmentClient;
    FragmentListeClient fragmentListeClient;
    FragmentListeVendeur fragmentListeVendeur;
    private FragmentManager manager;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //par defaut l'application ne démare pas en mode admin
        estAdmin=false;
        bundle = new Bundle();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //on récupère le layout du toolbar
        toolbar = findViewById(R.id.toolbar);

        //assigne la toolbar comme action bar de l'app
        setSupportActionBar(toolbar);

        // instanciation du gestionnaire de fragments
        manager = getSupportFragmentManager();
        // association du gestionnaire des actions sur la stack
        manager.addOnBackStackChangedListener(this);
        //instanciation de la transaction pour les frangments
        FragmentTransaction transaction = manager.beginTransaction();
        //creation du fragment listeVendeur
        fragmentListeVendeur = new FragmentListeVendeur();
        //creation du fragment listeClient
        fragmentListeClient = new FragmentListeClient();

        //Instanciation de la BD
        mDb = ItemRoomDB.getDatabase(this);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                //on instancie la liste des Items vendeurs
                listeItemsVendeur =new ArrayList<Item>( mDb.ItemDao().getItems());
                if(listeItemsVendeur.isEmpty()){
                    //création de deux items
                    Item item1 = new Item("Balais","un simple balais","accessoires",12);
                    Item item2 = new Item("Pine sol","Produit nettoyant","savons",6);
                    //ajout de l'item1 à la BD
                    mDb.ItemDao().Insert(item1);
                    //ajout de l'item 2 à la BD
                    mDb.ItemDao().Insert(item2);
                    //on refait la requête pour assigner les valeurs
                    listeItemsVendeur =new ArrayList<Item>( mDb.ItemDao().getItems());


                }

                // on utilise le thread principal pour faire une MAJ de la vue
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //on passe la liste au bundle
                        bundle.putSerializable("listeItemsVendeur",listeItemsVendeur);
                        //passage du bundle en arguments au fragment liste vendeurs
                        fragmentListeVendeur.setArguments(bundle);
                        //ajout du fragment ListeVendeur dans le frame layout
                        transaction.add(R.id.fl_fragment1,fragmentListeVendeur);
                        //ajout du fragment ListeClient dans le frame layout
                        transaction.add(R.id.fl_fragment2,fragmentListeClient);
                        transaction.commit();


                    }
                });
            }


        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //creation de l'inflater du menu
        MenuInflater inflater = getMenuInflater();
        //on indique le layout du menu à inflater et l'objet menu qui sera la cible
        inflater.inflate(R.menu.menu_admin,menu);
        return true;
    }

    @Override
    //méthode qui gère lorsqu'on clique sur un item du menu
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //FragmentListeVendeur fragment;
        //fragment = (FragmentListeVendeur) getFragmentManager().findFragmentById(R.id.)
        FragmentTransaction transaction = manager.beginTransaction();
        switch (item.getItemId()){
            //si on clique sur l'item menu admin
            case R.id.admin:
                //si le mode admin est activé
                if (estAdmin){
                    //on passe la variable à false
                    estAdmin = false;
                    //Log.e("estAdmin", "onOptionsItemSelected: "+estAdmin);

                }
                //sinon...
                else{
                    //on passe la variable à vrai
                    estAdmin = true;
                    //Log.e("estAdmin", "onOptionsItemSelected: "+estAdmin);

                }
                FragmentListeVendeur fragmentListeVendeur = new FragmentListeVendeur();

                //on ajoute la valeur de "estAdmin" a l'objet Bundle
                bundle.putBoolean("estAdmin",estAdmin);
                //on envoie la valeur de la listeItemsVendeur au fragment
                bundle.putSerializable("listeItemsVendeur",listeItemsVendeur);
                fragmentListeVendeur.setArguments(bundle);
                //on remplace le fragment actuellement placé dans le frame layout par celui contenant le nouveau bundle
                transaction.replace(R.id.fl_fragment1,fragmentListeVendeur);
                transaction.commit();






         }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        if(manager.getBackStackEntryCount()>0){

        }
        else{}

    }
}